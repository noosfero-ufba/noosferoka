# Tema Educanal

Configurações manuais pós instalação do tema a serem feitas na comunidade /educanal:

## 01. Criar um bloco html puro com o seguinte conteúdo:

```html
<ul class="mais-transmissoes">
  <li>
    <a href="/educanal/auditorio-01">
      <img src="/designs/themes/educanal/images/auditorio-01.svg" />
    </a>
  </li>
  <li>
    <a href="/educanal/auditorio-02">
      <img src="/designs/themes/educanal/images/auditorio-02.svg" />
    </a>
  </li>
  <li>
    <a href="http://200.128.51.113:8080/alt">
      <img src="/designs/themes/educanal/images/polemicas-contemporaneas.svg" />
    </a>
  </li>
  <li>
    <a href="/educanal/externo">
      <img src="/designs/themes/educanal/images/externo.svg" />
    </a>
  </li>
</ul>
```

## 02. Criar conteúdos

* /educanal/auditorio-01
  * Copiar player de https://blog.ufba.br/radiofaced/?p=1255
* /educanal/auditorio-02
  * Copiar player de https://blog.ufba.br/radiofaced/?p=1406
* /educanal/externo
  * Copiar player de https://blog.ufba.br/radiofaced/?p=1417
* /educanal/notícias
* /educanal/sobre

## 03. Criar bloco de links

Criar bloco de links abaixo do bloco HTML com os links

* Como fazer uma transmissão
* Projetos parceiros

## 04. Adicionar bloco de membros na lateral esquerda

Remover todos os blocos da lateral esquerda e manter apenas a lista de membros.
